package com.example.projetandroid;

public interface ClickListener {
    void onClick(int position);
}
