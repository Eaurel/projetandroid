package com.example.projetandroid;

import com.example.projetandroid.models.ResultPopularMovie;
import com.example.projetandroid.modelsdetails.MovieDetails;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieDBClient {

    @GET("popular")
    Call<ResultPopularMovie> FilmPopulaire(
            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") int page
    );

    @GET("movie")
    Call<ResultPopularMovie> RechercheFilm(
      @Query("api_key") String api_key,
      @Query("language") String language,
      @Query("query") String query,
      @Query("page") int page,
      @Query("include_adult") boolean include_adult
    );

    @GET("{id}")
    Call<MovieDetails> details(
            @Path("id") int id,
            @Query("api_key") String api_key,
            @Query("language") String language
    );

}
