package com.example.projetandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projetandroid.modelsdetails.MovieDetails;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class filmDetail extends AppCompatActivity {

    private String apiKey = "7a4643e6fe6d50a2f73b9bf21a26eb85";
    private TextView titre;
    private ImageView poster;
    private TextView date;
    private TextView titreOriginal;
    private TextView genres;
    private TextView description;
    private String stringGenre = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_detail);

        Intent intentRecu = getIntent();
        int id = Integer.parseInt(intentRecu.getSerializableExtra("id").toString());

        titre = findViewById(R.id.Titre);
        poster = findViewById(R.id.poster);
        date = findViewById(R.id.date);
        titreOriginal = findViewById(R.id.TitreOriginal);
        genres = findViewById(R.id.genres);
        description = findViewById(R.id.Description);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/movie/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        MovieDBClient client = retrofit.create(MovieDBClient.class);
        Call<MovieDetails> call = client.details(id,apiKey,"fr-FR");

        call.enqueue(new Callback<MovieDetails>() {
            @Override
            public void onResponse(Call<MovieDetails> call, Response<MovieDetails> response) {
                final MovieDetails films = response.body();
                titre.setText(films.getTitle());
                date.setText(films.getReleaseDate());
                titreOriginal.setText(films.getOriginalTitle());
                description.setText(films.getOverview());
                for(int i=0;i<films.getGenres().size();i++) {
                    stringGenre = stringGenre + films.getGenres().get(i).getName() + " ";
                }
                genres.setText(stringGenre);
                new com.example.projetandroid.DownloadImageTask(poster)
                        .execute("https://image.tmdb.org/t/p/w780" + films.getPosterPath());
            }

            @Override
            public void onFailure(Call<MovieDetails> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Error...!!!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.info_app, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_link:
                finish();
                Intent intent_home = new Intent(this, MainActivity.class);
                startActivity(intent_home);
                return true;
            case R.id.searchMenu:
                finish();
                Intent intent_search = new Intent(this, recherchefilm.class);
                startActivity(intent_search);
                return true;
            case R.id.informations:
                finish();
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
