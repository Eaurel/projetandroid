package com.example.projetandroid;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projetandroid.models.ResultPopularMovie;

import java.net.MalformedURLException;

public class filmPopulaireAdapter extends RecyclerView.Adapter<filmPopulaireViewHolder> {
    private ResultPopularMovie films;
    private ClickListener listener;

    public filmPopulaireAdapter(ResultPopularMovie newFilms,ClickListener listener)
    {
        this.films = newFilms;
        this.listener = listener;
    }

    @NonNull
    @Override
    public filmPopulaireViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filmpopulaire,parent,false);
        return new filmPopulaireViewHolder(view,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull filmPopulaireViewHolder holder, int position) {
        try {
            holder.display(films.getResults().get(position));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return this.films.getResults().size();
    }

}
