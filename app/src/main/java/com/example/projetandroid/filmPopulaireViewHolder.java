package com.example.projetandroid;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projetandroid.models.Result;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;

public class filmPopulaireViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView Titre;
    private ImageView Image;
    private TextView description;
    private WeakReference<ClickListener> listenerRef;

    public filmPopulaireViewHolder(@NonNull View itemView,ClickListener listener)
    {
        super(itemView);

        listenerRef = new WeakReference<ClickListener>(listener);
        Titre = itemView.findViewById(R.id.Titre);
        Image = itemView.findViewById(R.id.poster);
        description = itemView.findViewById(R.id.description);

        Image.setOnClickListener(this);
        Titre.setOnClickListener(this);
    }

    public void display(Result film) throws MalformedURLException {
        new com.example.projetandroid.DownloadImageTask(Image)
               .execute("https://image.tmdb.org/t/p/w780" + film.getBackdropPath());
        Titre.setText(film.getTitle());
        description.setText(film.getOverview());
    }

    @Override
    public void onClick(View v) {
        listenerRef.get().onClick(getAdapterPosition());
    }
}
