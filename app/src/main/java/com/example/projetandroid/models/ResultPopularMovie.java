
package com.example.projetandroid.models;

import java.io.Serializable;
import java.util.List;

public class ResultPopularMovie implements Serializable
{

    private Integer page;
    private Integer totalResults;
    private Integer totalPages;
    private List<Result> results = null;
    private final static long serialVersionUID = 1297816216656862378L;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }
}
