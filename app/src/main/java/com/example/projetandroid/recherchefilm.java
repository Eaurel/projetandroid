package com.example.projetandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

public class recherchefilm extends AppCompatActivity {

    private EditText film;
    private String nomFilm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recherchefilm);

        film = findViewById(R.id.film);
    }


    public void recherche(View v)
    {
        nomFilm = film.getText().toString();
        finish();
        Intent intent = new Intent(this,resultatRechercheFilm.class);
        intent.putExtra("nom",nomFilm);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.info_app, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_link:
                finish();
                Intent intent_home = new Intent(this, MainActivity.class);
                startActivity(intent_home);
                return true;
            case R.id.searchMenu:
                finish();
                Intent intent_search = new Intent(this, recherchefilm.class);
                startActivity(intent_search);
                return true;
            case R.id.informations:
                finish();
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
