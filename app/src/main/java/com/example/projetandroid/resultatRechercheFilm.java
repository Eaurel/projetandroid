package com.example.projetandroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.projetandroid.models.ResultPopularMovie;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class resultatRechercheFilm extends AppCompatActivity {

    private RecyclerView recycler;
    private int page = 1;
    private String apiKey = "7a4643e6fe6d50a2f73b9bf21a26eb85";
    private String nomFilm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultat_recherche_film);

        Intent intentRecu = getIntent();
        nomFilm = (String) intentRecu.getSerializableExtra("nom");
        recycler = findViewById(R.id.RecyclerView);
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/search/")
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        MovieDBClient client = retrofit.create(MovieDBClient.class);
        Call<ResultPopularMovie> call = client.RechercheFilm(apiKey,"fr-FR",nomFilm,page,false);
        //premier chargement de la page
        call.enqueue(new Callback<ResultPopularMovie>() {
            @Override
            public void onResponse(Call<ResultPopularMovie> call, Response<ResultPopularMovie> response) {
                final ResultPopularMovie films = response.body();
                recycler.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                recycler.setAdapter(new filmPopulaireAdapter(films, new ClickListener() {
                    @Override
                    public void onClick(int position) {
                        Intent intent = new Intent(getApplicationContext(), filmDetail.class);
                        intent.putExtra("id",films.getResults().get(position).getId());
                        startActivity(intent);
                    }
                }));
            }

            @Override
            public void onFailure(Call<ResultPopularMovie> call, Throwable t) {
                Toast.makeText(resultatRechercheFilm.this, "Error...!!!", Toast.LENGTH_SHORT).show();
            }
        });

        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                //si on est en bas de la page, alors on fais page+1 pour charger le reste
                //on reprend le même code que sur le chargement du json
                if (!recyclerView.canScrollVertically(1)) {
                    page++;
                    Retrofit.Builder builder = new Retrofit.Builder()
                            .baseUrl("https://api.themoviedb.org/3/search/")
                            .addConverterFactory(GsonConverterFactory.create());
                    Retrofit retrofit = builder.build();
                    MovieDBClient client = retrofit.create(MovieDBClient.class);
                    Call<ResultPopularMovie> call = client.RechercheFilm(apiKey,"fr-FR",nomFilm,page,false);
                    call.enqueue(new Callback<ResultPopularMovie>() {
                        @Override
                        public void onResponse(Call<ResultPopularMovie> call, Response<ResultPopularMovie> response) {
                            final ResultPopularMovie films = response.body();
                            recycler.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                            recycler.setAdapter(new filmPopulaireAdapter(films, new ClickListener() {
                                @Override
                                public void onClick(int position) {
                                    Intent intent = new Intent(getApplicationContext(), filmDetail.class);
                                    intent.putExtra("id",films.getResults().get(position).getId());
                                    startActivity(intent);
                                }
                            }));
                        }
                        @Override
                        public void onFailure(Call<ResultPopularMovie> call, Throwable t) {
                            Toast.makeText(resultatRechercheFilm.this, "Error...!!!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.info_app, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_link:
                finish();
                Intent intent_home = new Intent(this, MainActivity.class);
                startActivity(intent_home);
                return true;
            case R.id.searchMenu:
                finish();
                Intent intent_search = new Intent(this, recherchefilm.class);
                startActivity(intent_search);
                return true;
            case R.id.informations:
                finish();
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
